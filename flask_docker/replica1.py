from flask import Flask, jsonify
import json

app = Flask(__name__)


@app.route('/return_secret_number', methods=['GET'])
def return_secret_number():

    return jsonify(secret_number=sn)


if __name__ == "__main__":
    with open("secret_number.json", "r") as f:
        sn_dict = json.load(f)
    sn = sn_dict['secret_number']
    app.run(host='0.0.0.0', port=5001)
